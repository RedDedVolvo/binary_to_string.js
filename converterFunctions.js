function binaryToString(str) {
    // Removes the spaces from the binary string
    str = str.replace(/\s+/g, ''); // /\s means one space and /\s+/g means multiple space, ie: '  A B C  D EF  G" = ##A#B#C##D#EF##G" for 1, for 2 = "#A#B#C#EF#G"
    // Pretty (correct) print binary (add a space every 8 characters)
    str = str.match(/.{1,8}/g).join(" ");

    var newBinary = str.split(" ");
    var binaryCode = [];

    for (i = 0; i < newBinary.length; i++) {
        binaryCode.push(String.fromCharCode(parseInt(newBinary[i], 2)));
    }
    
    return binaryCode.join("");
}


function stringToBinary(str, spaceSeparatedOctets) {

    var completedString = '';

    function zeroPad(num) {
        return "00000000".slice(String(num).length) + num;
    }

    return str.replace(/[\s\S]/g, function(str) {
        str = zeroPad(str.charCodeAt().toString(2));
        return !1 == spaceSeparatedOctets ? str : str + " "
    });
}

// Let's create a function we can import to the HTML utilizing the
// other scripts!

function validateInput() {
    var nameField = document.getElementById(word_string);
    var binaryField = document.getElementById(binary);
            
    // debug a bit
    console.log("This is working");

    // Let's set restrictions to what can be placed in this input
    if (nameField != null) {
        stringToBinary(str);
    } else { 
        alert("You need to enter someting in the String field!")
    }

    // Let's set restrictions to what can be placed in this input
    var x = document.forms['converter']['binary'].value;
    var regex=/^[0-1]+" "/;
    if (x.match(regex))
    {
        alert("Must be binary sequence");
        return false;
    } else {
        binaryToString();
    }
}
